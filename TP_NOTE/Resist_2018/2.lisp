(defun tri_rec(x)
	(cond
		((eq x 0) 0)
		(t (+ x (tri_rec (- x 1))))
	)
)

(defun triangle(a x)
	(eq (tri_rec x) a)
)

(triangle 5 2)
(triangle 6 3)

(defun multiplier(l)
	(cond
		((null l) 1)
		((numberp (car l)) (* (car l) (multiplier (cdr l))))
		(t (* (multiplier (car l)) (multiplier (cdr l))))
	)
)

(multiplier '(2 3 -4 (5 5) 2))

(defun somme(l)
	(cond
		((null l) 0)
		((numberp (car l)) (+ (car l) (somme (cdr l))))
		((consp (car l)) (+ (somme (car l)) (somme (cdr l))))
		(t (somme (cdr l)))
	)
)

(somme '(2 2 -4 (5 5) boo foo 2))