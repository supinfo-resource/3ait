def as_much_as(l):
    return l.count('a') == l.count('b')

print(as_much_as(['a','a','b','b']))
print(as_much_as(['a','b','b','b']))