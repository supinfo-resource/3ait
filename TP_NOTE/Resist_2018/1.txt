soit a = 14 dans
    soit a = 20, b = a+12
        dans a+b 

Deux réponses sont possible. je peux obtenir: 

a1 = 14; a2 = 20; b = a1+12; res = a2+b donc 14+12+20 = 36.
Ou bien 
a1 = 14; a2 = 20; b = a2 + 12; res = a2+b donc 20+20+12 = 52.

La première solution est l'interprétation la plus naturelle de la formule. La deuxième correspond à une interpration ou la valeur de la variable est modifié directement après sont énonciation.